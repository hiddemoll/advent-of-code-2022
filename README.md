# Advent of Code

This repository contains my solutions for Advent of Code, written in ***Haskell***. 

I consider myself as a beginner in Haskell, especiall with project management. 
Although I also try to solve AoC in ***Python***, I'd like to improve my ***Haskell***
skills. Tips and tops are therefore welcome.

This projects structure, although simplified, was based of [this repo](https://github.com/mstksg/advent-of-code-2022). It is currently very barebone, but I wish to add:
- Execute similar to example repo (with run and test)
- Use cabal instead of stack
- Use a logical structure for each day's solutions


### Daily themes
1. `**` Map functions over nested list

### Lessons learnt
- `eval` is a fun function for AoC
