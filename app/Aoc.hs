import AOC

main :: IO ()
main = do
  dat <- readFile "input/input01.txt"
  print "Day01 Part 1: " <> show day01a dat