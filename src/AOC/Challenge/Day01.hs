module AOC.Challenge.Day01 (
    day01a
  -- , day01b
  ) where

import Data.List (sort)
import Data.List.Split (splitWhen)

day01a :: [Char] -> Int
day01a = maximum . map sum $ calories
 where
  calories = map (map (read @Int)) $ splitWhen null $ lines . splitOn "\n\n"